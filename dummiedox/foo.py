__author__ = 'Sasho'

def foofunc(a, b):
""" This is a foo func.

:param a: param a
:param b: param b
:return: something cool
:type a: str
:type b: bool
:rtype: str
"""
    return a if b else 'FOO'
